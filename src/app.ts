import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import * as morgan from 'morgan';
import { WinstonLogger } from './config/winston';
const logger = new WinstonLogger().logger
import { AppRoutes } from './routes/app.routes';

class App {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();
        this.mongooseSetup();
    }

    private config() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(morgan('combined', { stream: { write: (message, encoding) => console.log(message) } }));
    }

    private routes() {
        this.app.use(new AppRoutes().router);
    }

    private mongooseSetup() {
        mongoose.connect('mongodb://localhost:27017/nextodevdb', { useNewUrlParser: true })
            .then(() => logger.info(`mongoosee connected`))
            .catch(() => logger.info(`ERROR! doesn't connected`));
    }
}

export default new App().app;