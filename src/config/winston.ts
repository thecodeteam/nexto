import * as appRoot from 'app-root-path';
import { Logger, createLogger, transports } from 'winston';

export class WinstonLogger {

    public logger: Logger;

    constructor() {
        this.config();
    }

    private config() {
        const options = {
            file: {
                level: 'info',
                filename: `${appRoot}/logs/app.log`,
                handleExceptions: true,
                json: true,
                maxSize: 5242880,
                maxFiles: 5,
                colorize: true
            },
            console: {
                level: 'info',
                handleExceptions: true,
                json: false,
                colorize: true,
                prettyPrint: true
            }
        }

        this.logger = createLogger({
            transports: [
                new transports.File(options.file),
                new transports.Console(options.console),
            ],
            exitOnError: false
        });
    }
}