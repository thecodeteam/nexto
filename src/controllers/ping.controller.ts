import { Request, Response, NextFunction } from 'express';
import { PingService } from '../services/ping.service';
import { WinstonLogger } from '../config/winston';
const logger = new WinstonLogger().logger;

export class PingController {

    public static getPing(req: Request, res: Response, next: NextFunction) {
        logger.info('CTRL: Starting getPing method');

        PingService.getPing()
            .then(response => {
                logger.info(`Response: ${JSON.stringify(response)}`);

                logger.info('CRTL: Ending getPing method');
                return res.status(200).send({response});
            })
            .catch(err => res.status(500).send({message: err}));
    }
    
}