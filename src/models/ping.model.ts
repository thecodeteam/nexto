import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const PingSchema = new Schema({
    ping: {
        type: Number
    }
});