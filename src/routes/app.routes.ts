import * as express from 'express';
import { PingRoutes } from './ping.routes';


export class AppRoutes {

    public router: express.Router;

    constructor() {
        this.config();
    }

    public config() {
        this.router = express.Router();
        this.router.use('/ping', new PingRoutes().router);
    }
}