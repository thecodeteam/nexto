import * as express from 'express';
import { PingController } from '../controllers/ping.controller';

export class PingRoutes {

    public router: express.Router;

    constructor() { 
        this.config();
    }

    public config() {
        this.router = express.Router();
        this.router.get('', PingController.getPing);
    }
}