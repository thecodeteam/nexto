import * as mongoose from 'mongoose';
import { PingSchema } from '../models/ping.model';
import { WinstonLogger } from '../config/winston';
const logger = new WinstonLogger().logger;
const Ping = mongoose.model('Ping', PingSchema);

export class PingService {

    public static getPing() {
        logger.info('SERVICE: Starting getPing method');
        const ping = new Ping({ping: 1});
        return ping.save();
    }

}